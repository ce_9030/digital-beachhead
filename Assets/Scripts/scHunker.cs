using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scHunker : MonoBehaviour
{
    #region Components
    Rigidbody2D movement;
    #endregion
    #region Prefabs 
    public GameObject pawn;
    public Transform bulletPrefab;
    public Transform roundPrefab;
    public Transform deathRingFX;
    public Transform deathExplosion;
    public Transform deathExplosionLarge;
    #endregion
    #region Fight Phase General Variables
    public enum FightPhase { PhaseOne, Intermission, PhaseTwo, Dead }
    public FightPhase currentPhase;
    public bool intermissionComplete;
    #endregion
    #region Intermission Variables
    public float pawnSpawnDelay;
    float timeSinceLastPawnSpawn;
    float interTime = 0;
    float interRotSpd = 1;
    #endregion
    #region Movement and Misc Variables 
    public float speed;
    float deathTime=0;
    float expTime = 0;
    bool largeExplosionCreated = false;
    #endregion
    #region Combat Variables
    float timeSinceLastShot;
    float timeSinceLastAttack;
    public float shotCount = 0;
    bool isAttacking;
    float attackSelect = 0;
    #region Sounds
    AudioSource audioData;
    public AudioClip shootSound;
    public AudioClip shootSound2;
    public AudioClip shootSound3;
    public AudioClip shootSound4;
    #endregion
    //public float phaseOneShotDelay;
    //public float phaseOneShotSpeed;
    #endregion
    void Start()
    {
        movement = GetComponent<Rigidbody2D>();
        audioData = GetComponent<AudioSource>();
        currentPhase = FightPhase.PhaseOne;
    }

    // Update is called once per frame
    void Update()
    {
        CheckPhaseChange();
        switch (currentPhase)
        {
            case FightPhase.PhaseOne:
                PhaseOneBehaviour();
                break;
            case FightPhase.Intermission:
                IntermissionBehaviour();
                break;
            case FightPhase.PhaseTwo:
                PhaseTwoBehaviour();
                break;
            case FightPhase.Dead:
                Die();
                break;
        }
    }

    void CheckPhaseChange()
    {
        // change to intermission at half health
        if (currentPhase == FightPhase.PhaseOne && GetComponent<scEnemyBase>().currentHP <= (GetComponent<scEnemyBase>().enemyHP / 2))
        {
            //transform.position = new Vector3(-7f, 0, 0);
            currentPhase = FightPhase.Intermission;
        }
        // change to phase two if intermission is complete
        else if (currentPhase == FightPhase.Intermission && intermissionComplete)
        {
            currentPhase = FightPhase.PhaseTwo;
        }
        else if (currentPhase == FightPhase.PhaseTwo && GetComponent<scEnemyBase>().currentHP<1)
        {
            currentPhase = FightPhase.Dead;
        }
    }

    void PhaseOneBehaviour()
    {
        //transform.position = Vector3.MoveTowards(transform.position, GameObject.Find("playercraft").transform.position, (speed/3 * Time.deltaTime));
        RotateToPlayer();
        movement.velocity = transform.right * speed;
        if (timeSinceLastAttack > 1.5f && isAttacking == false)
        {
            attackSelect = Mathf.Round(Random.Range(1,5));
            isAttacking = true;
            timeSinceLastAttack = 0;
            shotCount = 0;
        }
        else if (isAttacking == false) timeSinceLastAttack += Time.deltaTime;

        //Attack 1 - Layered Shots
        if (isAttacking == true && attackSelect == 1)
        {
            if (shotCount < 16 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.2f && shotCount<16)
                {
                    shotCount++;
                    layeredShots(transform.position + new Vector3(Random.Range(-0.6f, 0.6f), Random.Range(-0.6f, 0.6f)));
                    timeSinceLastShot = 0;
                }
            }
            else if (shotCount >= 16) isAttacking = false;
        }

        //Attack 2 - Arch Shot
        if (isAttacking == true && attackSelect == 2)
        {
            //float angle = -2f;
            if (shotCount < 20 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.1f && shotCount < 20)
                {
                    shotCount++;
                    arcShot(shotCount/7);
                    //angle += 1;
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 20) isAttacking = false;
        }

        //Attack 3 - Speed Burst
        if (isAttacking == true && attackSelect == 3)
        {
            if (shotCount < 9 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.1f && shotCount < 9)
                {
                    shotCount++;
                    speedBurst(shotCount*1.5f,false,false);
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 9) isAttacking = false;
        }

        //Attack 4 - Spread Radius
        if (isAttacking == true && attackSelect == 4)
        {
            if (shotCount < 8 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.4f && shotCount < 8)
                {
                    shotCount++;
                    spreadRadius(transform.position+new Vector3(Random.Range(-1f,1f),Random.Range(-1f,1f)),5f);
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 8) isAttacking = false;
        }
    }

    void IntermissionBehaviour()
    {
        if (interTime > 10f) //(transform.position.x >= 7)
        {
            intermissionComplete = true;
            movement.velocity = Vector3.zero;
        }
        else interTime += Time.deltaTime;
        //movement.velocity = Vector3.right;

        if (interTime < 1f) { interRotSpd += 0.0025f; movement.velocity /= 3; }
        else if (interTime > 9f) interRotSpd -= 0.0025f;
        transform.rotation *= Quaternion.Euler(0, 0, interRotSpd);

        if (timeSinceLastPawnSpawn >= pawnSpawnDelay && interTime>1f && interTime<9f)
        {
            audioData.PlayOneShot(shootSound3, 1f);
            timeSinceLastPawnSpawn = 0;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (i == 0 && j == 0) continue;
                    GameObject newPawn = Instantiate(pawn, transform.position, transform.rotation);
                    newPawn.GetComponent<Rigidbody2D>().velocity = (transform.right*(i*2)) + (transform.up*(j*2));
                    newPawn.GetComponent<scPawn>().targetTransform = gameObject.transform.Find("playercraft");
                }
            }
        }
        else timeSinceLastPawnSpawn += Time.deltaTime;
    }

    void PhaseTwoBehaviour()
    {
        RotateToPlayer();
        movement.velocity = transform.right * (speed*1.1f);
        if (timeSinceLastAttack > 1f && isAttacking == false)
        {
            attackSelect = Mathf.Round(Random.Range(1, 8));
            isAttacking = true;
            timeSinceLastAttack = 0;
            shotCount = 0;
        }
        else if (isAttacking == false) timeSinceLastAttack += Time.deltaTime;

        //Attack 1 - Layered Shots PLUS
        if (isAttacking == true && attackSelect == 1)
        {
            if (shotCount < 20 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.15f && shotCount < 20)
                {
                    shotCount++;
                    layeredShots(transform.position + new Vector3(Random.Range(-0.7f, 0.7f), Random.Range(-0.7f, 0.7f)));
                    timeSinceLastShot = 0;
                }
            }
            else if (shotCount >= 20) isAttacking = false;
        }

        //Attack 2 - Arch Shot PLUS
        if (isAttacking == true && attackSelect == 2)
        {
            if (shotCount < 26 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.1f && shotCount < 26)
                {
                    shotCount++;
                    arcShot(shotCount / 5);
                    //angle += 1;
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 26) isAttacking = false;
        }

        //Attack 3 - Speed Burst PLUS
        if (isAttacking == true && attackSelect == 3)
        {
            if (shotCount < 20 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.1f && shotCount < 20)
                {
                    shotCount++;
                    speedBurst(shotCount, false,false);
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 20) isAttacking = false;
        }

        //Attack 4 - Spread Radius PLUS
        if (isAttacking == true && attackSelect == 4)
        {
            if (shotCount < 11 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.3f && shotCount < 11)
                {
                    shotCount++;
                    spreadRadius(transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)),5f);
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 11) isAttacking = false;
        }

        //Attack 5 - Speed Burst (Flank-Type)
        if (isAttacking == true && attackSelect == 5)
        {
            if (shotCount < 12 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.2f && shotCount < 12)
                {
                    shotCount++;
                    speedBurst(shotCount, false,true);
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 12) isAttacking = false;
        }

        //Attack 6 - Junction Shot
        if (isAttacking == true && attackSelect == 6)
        {
            if (shotCount < 9 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.5f && shotCount < 9)
                {
                    shotCount++;
                    junctionShot(Random.Range(-0.5f, 0.5f));
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 9) isAttacking = false;
        }

        //Attack 7 - Divider Radius
        if (isAttacking == true && attackSelect == 7)
        {
            if (shotCount < 11 && isAttacking)
            {
                timeSinceLastShot += Time.deltaTime;
                if (timeSinceLastShot > 0.3f && shotCount < 11)
                {
                    shotCount++;
                    dividerRadius(transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)), 3f, Random.Range(-1f, 1f));
                    timeSinceLastShot = 0;

                }
            }
            else if (shotCount >= 11) isAttacking = false;
        }
    }

    void Die()
    {
        GetComponent<PolygonCollider2D>().enabled = false;
        GetComponent<scEnemyBase>().currentHP = 0;
        movement.velocity = new Vector2(0, 0);
        if (deathTime < 0.01f) {
            Transform deathRing = Instantiate(deathRingFX, transform.position, transform.rotation) as Transform;
            deathRing.GetComponent<scPlayerDeath>().playDeathSound = false;
            deathRing.GetComponent<scPlayerDeath>().showMessage = false;
        }
        deathTime += Time.deltaTime;
        if (deathTime >0.001 && deathTime < 4) {
            expTime += Time.deltaTime;
            transform.localScale -= new Vector3(0.0005f, 0.0005f);
        }
        if (expTime>0.04f)
        {
            Transform explosion=Instantiate(deathExplosion, transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)), transform.rotation) as Transform;
            explosion.GetComponent<SpriteRenderer>().color = Color.red;
            transform.rotation = Quaternion.Euler(0, 0, Random.Range(-360, 360));
            expTime = 0;
        }
        if (deathTime > 4 && largeExplosionCreated==false)
        {
            Instantiate(deathExplosionLarge, transform.position, transform.rotation);
            global.kills += GetComponent<scEnemyBase>().killValue;
            global.score += GetComponent<scEnemyBase>().scoreValue;
            if (global.remain != 0) global.remain -= 1;
            global.weaponexp[global.currentweapon] += GetComponent<scEnemyBase>().expValue;
            largeExplosionCreated = true;
            expTime = 0;
        }
        if (deathTime > 5)
            Destroy(this.gameObject);
    }

    void RotateToPlayer()
    {
        Vector3 vectorToTarget = GameObject.Find("playercraft").transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 45f);
        GetComponent<Rigidbody2D>().velocity = transform.right * Random.Range(1, 2.5f);
    }

    void layeredShots(Vector2 origin)
    {
        audioData.PlayOneShot(shootSound2, 1f);
        float randomAngle = Random.Range(-1.8f, 1.8f);
        Transform newShot = Instantiate(bulletPrefab, origin, transform.rotation) as Transform;
        //Vector2 direction = new Vector2(GameObject.Find("playercraft").transform.position.x - transform.position.x, GameObject.Find("playercraft").transform.position.y - transform.position.y).normalized * phaseOneShotSpeed;
        newShot.GetComponent<Rigidbody2D>().velocity = (transform.right * 4) + (transform.up * randomAngle);
        newShot = Instantiate(bulletPrefab, origin, transform.rotation) as Transform;
        newShot.GetComponent<Rigidbody2D>().velocity = (transform.right * 5) + (transform.up * randomAngle);
        newShot = Instantiate(bulletPrefab, origin, transform.rotation) as Transform;
        newShot.GetComponent<Rigidbody2D>().velocity = (transform.right * 6) + (transform.up * randomAngle);
    }

    void arcShot(float angle)
    {
        audioData.PlayOneShot(shootSound, 1f);
        //Layer 1
        Transform bullet = Instantiate(roundPrefab, transform.position+new Vector3(Random.Range(-0.3f,0.3f),Random.Range(-0.3f,0.3f)), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right*3) + new Vector3(0, angle);
        bullet = Instantiate(roundPrefab, transform.position + new Vector3(Random.Range(-0.3f, 0.3f), Random.Range(-0.3f, 0.3f)), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 3) + new Vector3(0, angle * -1);
        //Layer 2
        bullet = Instantiate(roundPrefab, transform.position + new Vector3(Random.Range(-0.3f, 0.3f), Random.Range(-0.3f, 0.3f)), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 4) + new Vector3(0,angle + 0.5f);
        bullet = Instantiate(roundPrefab, transform.position + new Vector3(Random.Range(-0.3f, 0.3f), Random.Range(-0.3f, 0.3f)), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 4) + new Vector3(0,(angle + 0.5f) * -1);
    }

    void speedBurst(float speed, bool randomspeed,bool flank)
    {
        audioData.PlayOneShot(shootSound4, 1f);
        Transform bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = transform.right * (7 * (speed / 6));
        if (flank==true)
        {
            bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as Transform;
            bullet.GetComponent<Rigidbody2D>().velocity = transform.right * (7 * (speed / 6)) + (transform.up * 1f);
            bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as Transform;
            bullet.GetComponent<Rigidbody2D>().velocity = transform.right * (7 * (speed / 6)) + (transform.up * -1f);
            bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as Transform;
            bullet.GetComponent<Rigidbody2D>().velocity = transform.right * (7 * (speed / 6)) + (transform.up * 0.5f);
            bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as Transform;
            bullet.GetComponent<Rigidbody2D>().velocity = transform.right * (7 * (speed / 6)) + (transform.up * -0.5f);
        }
        else
        {
            bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as Transform;
            bullet.GetComponent<Rigidbody2D>().velocity = transform.right * (7 * (speed / 6)) + (transform.up * 0.6f);
            bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as Transform;
            bullet.GetComponent<Rigidbody2D>().velocity = transform.right * (7 * (speed / 6)) + (transform.up * -0.6f);
        }
    }

    void spreadRadius(Vector2 origin, float speed)
    {
        audioData.PlayOneShot(shootSound3, 1f);
        for (float i=-1.22f;i<1.22f;i+=0.33f)
        {
            Transform bullet = Instantiate(roundPrefab, origin,Quaternion.Euler(0,0,transform.rotation.z+Random.Range(-22.5f,22.5f))) as Transform;
            bullet.GetComponent<Rigidbody2D>().velocity = transform.right * (speed - Mathf.Abs(i)) + (transform.up*(i+ Random.Range(-0.5f, 0.5f)));
        }
    }

    void junctionShot(float randangle)
    {
        audioData.PlayOneShot(shootSound2, 1f);
        //First Layer
        Transform bullet = Instantiate(bulletPrefab, new Vector2(transform.position.x, transform.position.y), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 4 + transform.up * randangle * 1.5f);
        bullet = Instantiate(bulletPrefab, new Vector2(transform.position.x, transform.position.y), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 4.5f + transform.up * randangle * 1.5f);
        bullet = Instantiate(bulletPrefab, new Vector2(transform.position.x, transform.position.y), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 5f + transform.up * randangle * 1.5f);

        //Second Layer
        bullet = Instantiate(bulletPrefab, new Vector2(transform.position.x, transform.position.y), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 4 + transform.up * randangle * -1.5f);
        bullet = Instantiate(bulletPrefab, new Vector2(transform.position.x, transform.position.y), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 4.5f + transform.up * randangle * -1.5f);
        bullet = Instantiate(bulletPrefab, new Vector2(transform.position.x, transform.position.y), transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * 5f + transform.up * randangle * -1.5f);
    }

    void dividerRadius(Vector2 origin, float speed,float randangle)
    {
        audioData.PlayOneShot(shootSound3, 1f);
        //First Layer
        Transform bullet = Instantiate(roundPrefab, origin, transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * speed) + ((transform.up + new Vector3(0,randangle)) /0.6f);
        bullet = Instantiate(roundPrefab, origin, transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * speed) + ((transform.up + new Vector3(0, randangle)) / -0.6f);
        //Second Layer
        bullet = Instantiate(roundPrefab, origin, transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * (speed+0.5f)) + ((transform.up + new Vector3(0, randangle)) / 0.65f);
        bullet = Instantiate(roundPrefab, origin, transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * (speed+0.5f)) + ((transform.up + new Vector3(0, randangle)) / -0.65f);
        //Third Layer
        bullet = Instantiate(roundPrefab, origin, transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * (speed+1f)) + ((transform.up + new Vector3(0, randangle)) / 0.7f);
        bullet = Instantiate(roundPrefab, origin, transform.rotation) as Transform;
        bullet.GetComponent<Rigidbody2D>().velocity = (transform.right * (speed+1f)) + ((transform.up + new Vector3(0, randangle)) / -0.7f);
    }
}
