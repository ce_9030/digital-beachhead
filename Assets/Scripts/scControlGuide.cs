using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scControlGuide : MonoBehaviour
{
    SpriteRenderer render;
    float opacity = 0.9f;
    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (global.remain < 160) opacity -= 0.01f;
        if (opacity < 0) Destroy(this.gameObject);
        render.color = new Color(1, 1, 1, opacity);
    }
}
