using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scGameOver : MonoBehaviour
{
    SpriteRenderer render;
    public bool playSound=false;
    public AudioClip appearSound;
    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<SpriteRenderer>();
        if (playSound) GetComponent<AudioSource>().PlayOneShot(appearSound,1f);
    }

    // Update is called once per frame
    void Update()
    {
        // list of object names that you want to destroy


        if (render.color.a > 0.9 && Input.GetMouseButton(0))
        {
            if (GameObject.Find("playercraft") == null && GameObject.Find("GlobalDefine") == null && GameObject.Find("Background") == null
                && GameObject.Find("enemySpawner") == null && GameObject.Find("spacebg") == null && GameObject.Find("grid") == null)
                SceneManager.LoadScene("Game_FieldA", LoadSceneMode.Single);
            else
            {
                string[] destroyTargets = new string[] { "playercraft", "GlobalDefine", "Background", "enemySpawner", "spacebg", "grid" };
                foreach (string name in destroyTargets)
                {
                    GameObject go = GameObject.Find(name);
                    //if the tree exist then destroy it
                    if (go)
                        Destroy(go.gameObject);
                }
            }
            
        }
        else if (render.color.a > 0.9 && Input.GetMouseButton(1))
        {
            if (GameObject.Find("playercraft") == null && GameObject.Find("GlobalDefine") == null && GameObject.Find("Background") == null
    && GameObject.Find("enemySpawner") == null && GameObject.Find("spacebg") == null && GameObject.Find("grid") == null)
                SceneManager.LoadScene("Start_Scene", LoadSceneMode.Single);
            else
            {
                string[] destroyTargets = new string[] { "playercraft", "GlobalDefine", "Background", "enemySpawner", "spacebg", "grid" };
                foreach (string name in destroyTargets)
                {
                    GameObject go = GameObject.Find(name);
                    //if the tree exist then destroy it
                    if (go)
                        Destroy(go.gameObject);
                }
            }
        }
        else render.color += new Color(0, 0, 0, 0.02f);
    }
}
