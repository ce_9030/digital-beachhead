using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scMenuButton : MonoBehaviour
{
    public Transform screenWipe;
    bool clicked = false;

    // Update is called once per frame

    void Start()
    {
        Application.targetFrameRate = 450;
    }
    void OnMouseDown()
    {
        if (clicked==false) {
        Transform trans = Instantiate(screenWipe, new Vector2(7.907888f, 0.09711881f), transform.rotation) as Transform;
            trans.GetComponent<scTransition>().roomselect = 1;
        clicked = true;
        }
    }
}
