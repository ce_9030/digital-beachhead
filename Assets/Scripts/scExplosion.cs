using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scExplosion : MonoBehaviour
{
    AudioSource audioData;
    public AudioClip expSound;
    public AudioClip expSound2;
    public bool disableAudio;
    public int mode = 0;
    // Start is called before the first frame update
    void Start()
    {
        disableAudio = false;
        Quaternion randomangle = Quaternion.Euler(0f, 0f, UnityEngine.Random.Range(-360f, 360f));
        transform.rotation = randomangle;
        audioData = GetComponent<AudioSource>();
        if (mode == 1) {transform.localScale = new Vector3(1.5f, 1.5f);
        if (disableAudio == false)
        AudioSource.PlayClipAtPoint(expSound2, new Vector3(transform.position.x, transform.position.y, transform.position.z));
        }
        else if (disableAudio==false && mode==0)
        AudioSource.PlayClipAtPoint(expSound,new Vector3(transform.position.x,transform.position.y,transform.position.z));
    }

    public void DestroyThis()
    {
        Destroy(this.gameObject);
    }
}
