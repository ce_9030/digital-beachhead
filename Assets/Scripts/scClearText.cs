using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scClearText : MonoBehaviour
{
    public Text content;
    public int mode; //1: Clear bonus. 2: Enemies Downed. 3: Lives Kept. 4: Total Score.
    // Start is called before the first frame update
    void Start()
    {
        content = GetComponent<Text>();
        if (mode==4) global.score += (3000 + (global.kills * 5) + (global.life * 1000));
        GameObject.Find("playercraft").GetComponent<CircleCollider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        content.color += new Color(0, 0, 0, 0.02f);
        switch (mode)
        {
            case 1: content.text = "3000"; break;
            case 2: content.text = global.kills.ToString()+"*5"; break;
            case 3: content.text = global.life.ToString()+"*1000"; break;
            case 4: content.text = global.score.ToString(); break;
        }
    }
}
