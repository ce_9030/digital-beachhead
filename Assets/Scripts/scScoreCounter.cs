using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scScoreCounter : MonoBehaviour
{
    public Text displayScore;
    public int mode = 0; //0: Score. 1: Hits.
    // Start is called before the first frame update
    void Start()
    {
        displayScore = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mode==1) displayScore.text = global.kills.ToString();
        else displayScore.text = global.score.ToString();
    }
}
