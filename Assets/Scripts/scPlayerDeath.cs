using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scPlayerDeath : MonoBehaviour
{
    SpriteRenderer sprite;
    bool fadeEnable;
    public float delay = 1f;
    float opacity = 1;
    public float growRate = 0.05f;
    public Transform messagePrefab;
    float timePassed;
    public bool playDeathSound = true;
    public bool showMessage = true;

    AudioSource audioData;
    public AudioClip expSound;
    public AudioClip deathSound;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        audioData = GetComponent<AudioSource>();
        audioData.PlayOneShot(expSound, 1f);
        if (playDeathSound) audioData.PlayOneShot(deathSound, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        timePassed += Time.deltaTime;
        transform.localScale += new Vector3(growRate, growRate, 0f);
        if (timePassed>delay) fadeEnable = true;
        if (fadeEnable == true) { opacity -= 0.0025f; }
        sprite.color = new Color(1, 0, 0, opacity);
        if (timePassed > (delay+ 2f) && showMessage)
        {
            Instantiate(messagePrefab, new Vector2(0, 0), Quaternion.Euler(0,0,0));
            Destroy(gameObject);
        } else if (timePassed>(delay+2f)) Destroy(gameObject);


    }
}
