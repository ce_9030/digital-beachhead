using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scWeaponGauge : MonoBehaviour
{
    public int weapon; //0=front. 1=spread. 2=vulcan.
    public Transform gauge;
    public Sprite[] spriteArray;
    public SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        spriteRenderer.sprite = spriteArray[global.weaponlevel[weapon]-1];

        switch (global.weaponlevel[weapon])
        { case 1: gauge.transform.localScale = new Vector2(((float)global.weaponexp[weapon] /150), 1); break;
          case 2: gauge.transform.localScale = new Vector2(((float)global.weaponexp[weapon] /300), 1); break;
        }

        //Level Up
        if (global.weaponlevel[weapon] < 3  && global.weaponexp[weapon] >= 150*global.weaponlevel[weapon])
        { global.weaponlevel[weapon] += 1; global.weaponexp[weapon] = 0;
          GameObject.Find("playercraft").GetComponent<AudioSource>().PlayOneShot(GameObject.Find("playercraft").GetComponent<scPlayer>().levelSound, 1f);
            Transform levelText = Instantiate(GameObject.Find("playercraft").GetComponent<scPlayer>().levelPrefab, GameObject.Find("playercraft").transform.position, transform.rotation) as Transform;
            levelText.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0.15f);
            switch (weapon)
            {
                case 0: levelText.GetComponent<SpriteRenderer>().color = new Color(0.427451f, 0.8117648f, 0.9647059f,1); break;
                case 1: levelText.GetComponent<SpriteRenderer>().color = new Color(0.1333333f, 0.882353f, 0.4078432f, 1); break;
                case 2: levelText.GetComponent<SpriteRenderer>().color = new Color(0.9490197f, 0.4235294f, 0.3098039f, 1); break;
            }
        }
    }
}
